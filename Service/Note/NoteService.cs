using System;
using System.Collections.Generic;
using System.Linq;
using Data.Model;
using Data.Repositrory;
using Service.Note.ViewModel;

namespace Service.Note
{
  public class NoteService : INoteService 
  {
    private readonly IRepository<Data.Model.Note> _repository;

    public NoteService(IRepository<Data.Model.Note> repository)
    {
      _repository = repository;
    }

    public NoteViewModel GetNote(int id)
    {
      var note = _repository.GetById(id);
      if (note == null)
      {
        return null;
      }

      var noteViewModel = new NoteViewModel
      {
        Id = note.Id,
        Title = note.Title,
        Description = note.Description,
        UpdateDate = new DateTime(long.Parse(note.UpdatedDate)),
        CreatedDate = new DateTime(long.Parse(note.CreatedDate))
      };

      return noteViewModel;
    }

    public IEnumerable<NoteViewModel> GetAll()
    {
      var allNoteViewModels = _repository.Filter(x => x.StatusCode != StatusCode.Deleted).ToList().Select(x => new NoteViewModel
      {
        Id = x.Id,
        Title = x.Title,
        Description = x.Description,
        UpdateDate = new DateTime(long.Parse(x.UpdatedDate)),
        CreatedDate = new DateTime(long.Parse(x.CreatedDate))
      });
     
      return allNoteViewModels;
    }

    public void Delete(int id)
    {
      var note = _repository.GetById(id);

      if (note == null)
      {
        return;
      }
      
      note.StatusCode = StatusCode.Deleted;
      _repository.Update(note);
    }

    public void Update(NoteViewModel request)
    {
      var note = _repository.GetById(request.Id);

      if (note == null)
      {
        return;
      }

      note.UpdatedDate = DateTime.UtcNow.Ticks.ToString();
      note.Title = request.Title;
      note.Description = request.Description;
      note.StatusCode = StatusCode.Updated;
     
      _repository.Update(note);
    }

    public void Create(NoteViewModel request)
    {
      var dateTimeTicks = DateTime.UtcNow.Ticks.ToString();

      var note = new Data.Model.Note
      {
        StatusCode = StatusCode.New,
        CreatedDate = dateTimeTicks,
        UpdatedDate = dateTimeTicks,
        Title = request.Title,
        Description = request.Description,
        Guid = Guid.NewGuid().ToString("D")
      };
      
      _repository.Create(note);
    }
  }
}