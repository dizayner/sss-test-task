﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Service.Note.ViewModel
{
  public class NoteViewModel
  {
    public int Id { get; set; }

    [Required]
    [MaxLength(250)]
    [Display(Name = "Заголовок")]
    public string Title { get; set; }

    [MaxLength(1000)]
    [Display(Name = "Описание")]
    public string Description { get; set; }

    [Display(Name = "Дата создания")]
    public DateTime CreatedDate { get; set; }

    [Display(Name = "Дата редактирования")]
    public DateTime UpdateDate { get; set; }
  }
}