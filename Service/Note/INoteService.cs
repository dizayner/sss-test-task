﻿using System.Collections.Generic;
using Service.Note.ViewModel;

namespace Service.Note
{
  public interface INoteService
  {
    NoteViewModel GetNote(int id);
    IEnumerable<NoteViewModel> GetAll();
    void Delete(int id);
    void Update(NoteViewModel request);
    void Create(NoteViewModel request);
  }
}