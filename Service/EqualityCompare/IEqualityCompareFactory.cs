﻿using System;
using System.Collections.Generic;

namespace Service.EqualityCompare
{
  public interface IEqualityCompareFactory<T> : IEqualityComparer<T>
  {
    IEqualityComparer<T> Create(Func<T, T, bool> comparer);
  }
}
