using System;
using System.Collections.Generic;

namespace Service.EqualityCompare
{
  public class EqualityCompareFactory<T> : IEqualityCompareFactory<T>
  {
    private Func<T, T, bool> _comparer;

    public bool Equals(T x, T y)
    {
      return _comparer(x, y);
    }

    public int GetHashCode(T obj)
    {
      return 0;
    }

    public IEqualityComparer<T> Create(Func<T, T, bool> comparer)
    {
      _comparer = comparer;

      return this;
    }
  }
}