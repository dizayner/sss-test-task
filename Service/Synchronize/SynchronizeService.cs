﻿using System.Collections.Generic;
using System.Linq;
using Data.Model;
using Data.Repositrory;
using Service.CloudStorage;
using Service.EqualityCompare;

namespace Service.Synchronize
{
  public class SynchronizeService : ISynchronizeService
  {
    private readonly ICloudNoteStorage _cloudNoteStorage;
    private readonly IRepository<Data.Model.Note> _noteRepository;
    private readonly IEqualityCompareFactory<Data.Model.Note> _compareFactory;

    public SynchronizeService(ICloudNoteStorage cloudNoteStorage, IRepository<Data.Model.Note> noteRepository, IEqualityCompareFactory<Data.Model.Note> compareFactory)
    {
      _cloudNoteStorage = cloudNoteStorage;
      _noteRepository = noteRepository;
      _compareFactory = compareFactory;
    }

    public void Synchronize()
    {
      var localNotes = _noteRepository.GetAll();
      var remoteNotes = _cloudNoteStorage.Get();

      if (this.FirstSession(localNotes, remoteNotes))
      {
        return;
      }

      this.UpdateFromCloud(localNotes, remoteNotes);
      this.NewFromCloud(localNotes, remoteNotes);
      this.DeleteFromCloud(localNotes.Where(x => x.Synchronized), remoteNotes);
      this.DeleteLocal(localNotes.Where(x => x.StatusCode == StatusCode.Deleted));
       this.UpdateCloudStorage();
    }

    private void NewFromCloud(IQueryable<Data.Model.Note> localNotes, IEnumerable<Data.Model.Note> remoteNotes)
    {
      var newFromCloud = remoteNotes.Except(localNotes,
       _compareFactory.Create((x, y) => x.Guid.Equals(y.Guid)));

      if (newFromCloud.Any() == false)
      {
        return;
      }

      foreach (var cloudNote in newFromCloud)
      {
        _noteRepository.Create(cloudNote);
      }
    }

    private void UpdateFromCloud(IQueryable<Data.Model.Note> localNotes, IEnumerable<Data.Model.Note> remoteNotes)
    {
      var updatedFromCloud = remoteNotes.Intersect(localNotes,
       _compareFactory.Create((x, y) => x.Guid.Equals(y.Guid) && long.Parse(x.UpdatedDate) < long.Parse(y.UpdatedDate)));

      if (updatedFromCloud.Any() == false)
      {
        return;
      }

      foreach (var cloudNote in updatedFromCloud)
      {
        var localNote = _noteRepository.Filter(x => x.Guid.Equals(cloudNote.Guid)).FirstOrDefault();

        if (localNote == null)
        {
          continue;
        }

        localNote.UpdatedDate = cloudNote.UpdatedDate;
        localNote.Description = cloudNote.Description;
        localNote.Title = cloudNote.Title;

        _noteRepository.Update(localNote);
      }
    }

    private void DeleteFromCloud(IQueryable<Data.Model.Note> localNotes, IEnumerable<Data.Model.Note> remoteNotes)
    {
      var deleteFromCloud = localNotes.ToList().Except(remoteNotes, _compareFactory.Create((x, y) => x.Guid.Equals(y.Guid)));

      if (deleteFromCloud.Any() == false)
      {
        return;
      }

      foreach (var cloudNote in deleteFromCloud)
      {
        _noteRepository.Delete(cloudNote.Id);
      }
    }

    private void DeleteLocal(IEnumerable<Data.Model.Note> deleteLocalNotes)
    {
      if (deleteLocalNotes.Any() == false)
      {
        return;
      }

      foreach (var localNote in deleteLocalNotes)
      {
        _noteRepository.Delete(localNote.Id);
      }
    }

    private bool FirstSession(IQueryable<Data.Model.Note> localNotes, IEnumerable<Data.Model.Note> remoteNotes)
    {
      if (remoteNotes == null && localNotes.Any())
      {
        _cloudNoteStorage.Send(localNotes);

        return true;
      }

      return false;
    }

    private void UpdateCloudStorage()
    {
      var localNotes = _noteRepository.GetAll();

      foreach (var localNote in localNotes)
      {
        localNote.Synchronized = true;
        _noteRepository.Update(localNote);
      }

      _cloudNoteStorage.Send(localNotes.Select(x => new
      {
        Guid = x.Guid,
        Title = x.Title,
        Description = x.Description,
        UpdatedDate = x.UpdatedDate,
        CreatedDate = x.CreatedDate
      }));
    }
  }
}