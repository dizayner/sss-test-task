﻿namespace Service.Synchronize
{
  public interface ISynchronizeService
  {
    void Synchronize();
  }
}