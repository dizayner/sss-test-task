﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Data.Model;
using Data.Repositrory;

namespace Service
{
  public class EntityService<TEntity> : IEntityService<TEntity> where TEntity : class, IEntity
  {
    private readonly IRepository<TEntity> _repository;

    public EntityService(IRepository<TEntity> repository)
    {
      _repository = repository;
    }

    public virtual TEntity GetById(int id)
    {
      var entity = _repository.GetById(id);

      return entity;
    }

    public virtual void Delete(int id)
    {
      _repository.Delete(id);
    }

    public virtual void Create(TEntity entity)
    {
      _repository.Create(entity);
    }

    public virtual void Update(TEntity entity)
    {
      _repository.Update(entity);
    }

    public IQueryable<TEntity> GetAll()
    {
      var allEntities = _repository.GetAll();

      return allEntities;
    }

    public IQueryable<TEntity> Filter(Expression<Func<TEntity, bool>> condition)
    {
      var filteredEntities = _repository.Filter(condition);

      return filteredEntities;
    }
  }
}