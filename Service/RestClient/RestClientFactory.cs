﻿using RestSharp;

namespace Service.RestClient
{
  public class RestClientFactory : IRestClientFactory
  {
    public RestSharp.RestClient Create(string endPoint)
    {
      var restClient = new RestSharp.RestClient(endPoint);
      restClient.AddDefaultHeader("Content-type","application/json; charset=utf-8");

      return restClient;
    }
  }
}