﻿namespace Service.RestClient
{
  public interface IRestClientFactory
  {
    RestSharp.RestClient Create(string endPoint);
  }
}