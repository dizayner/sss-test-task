﻿using System.Collections.Generic;
using RestSharp;
using Service.RestClient;

namespace Service.CloudStorage
{
  public class CloudNoteStorage : ICloudNoteStorage
  {
    private readonly IRestClientFactory _restClientFactory;
    private readonly string EndPoint = "https://profigroup.by/applicants-tests/mobile/v2/kjdfg4jjf/";

    public CloudNoteStorage(IRestClientFactory restClientFactory)
    {
      _restClientFactory = restClientFactory;
    }

    public void Send(IEnumerable<object> notes)
    {
      var request = new RestRequest
      {
        Method = Method.PUT,
        RequestFormat = DataFormat.Json,
      };
      request.AddJsonBody(notes);

      _restClientFactory.Create(this.EndPoint).Execute(request);
    }

    public IEnumerable<Data.Model.Note> Get()
    {
      var request = new RestRequest
      {
        Method = Method.GET,
        RequestFormat = DataFormat.Json
      };

      var response = _restClientFactory.Create(this.EndPoint).Execute<List<Data.Model.Note>>(request);

      return response.Data;
    }
  }
}