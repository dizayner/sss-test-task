﻿using System.Collections.Generic;

namespace Service.CloudStorage
{
  public interface ICloudNoteStorage
  {
    void Send(IEnumerable<object> notes);
    IEnumerable<Data.Model.Note> Get();
  }
}
