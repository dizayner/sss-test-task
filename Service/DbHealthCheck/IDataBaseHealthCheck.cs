﻿namespace Service.DbHealthCheck
{
  public interface IDataBaseHealthCheck
  {
    bool IsOverflow();
  }
}
