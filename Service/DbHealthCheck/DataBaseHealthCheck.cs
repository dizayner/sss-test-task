using System.IO;

namespace Service.DbHealthCheck
{
  public class DataBaseHealthCheck : IDataBaseHealthCheck
  {
    private readonly string pathToDb;
    private readonly int maxFileSize = 1572864; //limit db to 1.5MB

    public DataBaseHealthCheck(string pathToDb)
    {
      this.pathToDb = pathToDb;
    }

    public bool IsOverflow()
    {
      var fileInfo = new FileInfo(pathToDb);

      //throw file not exist
      if (fileInfo.Exists == false)
      {
        return true;
      }

      return fileInfo.Length >= maxFileSize;
    }
  }
}