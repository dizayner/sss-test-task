using System.Data.Entity;
using Data;
using Data.Repositrory;
using Microsoft.Practices.Unity;
using Service;
using Service.CloudStorage;
using Service.DbHealthCheck;
using Service.EqualityCompare;
using Service.Note;
using Service.RestClient;
using Service.Synchronize;

namespace Web
{
  public class DependencyRegister
  {
    private IUnityContainer container;
    public DependencyRegister()
    {
      container = new UnityContainer();
      this.RegisterTypes(container);
    }

    public IUnityContainer GetConfiguredContainer()
    {
      return container;
    }

    public void RegisterTypes(IUnityContainer container)
    {
      this.DataLayerRegister(container);
      this.ServiceLayerRegister(container);
    }

    private void DataLayerRegister(IUnityContainer unityContainer)
    {
      container.RegisterType<DbContext, DataLocalContext>(new TransientLifetimeManager());
      container.RegisterType(typeof (IRepository<>), typeof (Repository<>));
    }

    private void ServiceLayerRegister(IUnityContainer unityContainer)
    {
      container.RegisterType(typeof(IEntityService<>), typeof(EntityService<>));
      container.RegisterType(typeof(IEqualityCompareFactory<>), typeof(EqualityCompareFactory<>));
      container.RegisterType<INoteService, NoteService>();
      container.RegisterType<IRestClientFactory, RestClientFactory>();  
      container.RegisterType<ICloudNoteStorage, CloudNoteStorage>();
      container.RegisterType<ISynchronizeService, SynchronizeService>();
      
     container.RegisterType<IDataBaseHealthCheck, DataBaseHealthCheck>(new InjectionConstructor(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/DataLocal.db")));
    }
  }
}
