﻿using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Service.DbHealthCheck;
using Service.Synchronize;
using Web.ViewModel.Shared;

namespace Web.Synchronize
{
  public class SynchronizeController : Controller
  {
    [Dependency]
    public ISynchronizeService SynchronizeService { get; set; }

    [Dependency]
    public IDataBaseHealthCheck DataBaseHealthCheck { get; set; }

    public ActionResult Synchronize()
    {

      if (DataBaseHealthCheck.IsOverflow())
      {
        var errorModel = new ErrorViewModel
        {
          Message = "Лимит на хранение в базе данных превышен.",
          BackAction = "Index",
          Controller = "Home"
        };

        return View("Error", errorModel);
      }

      this.SynchronizeService.Synchronize();

      return RedirectToAction("Index", "Home");
    }
  }
}