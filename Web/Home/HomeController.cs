﻿using System;
using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Service.DbHealthCheck;
using Service.Note;
using Service.Note.ViewModel;
using Web.ViewModel.Shared;

namespace Web.Home
{
  public class HomeController : Controller
  {
    [Dependency]
    public INoteService NoteService { get; set; }

    [Dependency]
    public IDataBaseHealthCheck DataBaseHealthCheck { get; set; }


    public ActionResult Index()
    {
      var allNotes = this.NoteService.GetAll();

      return View(allNotes);
    }

    public ActionResult ConfirmDelete(int id)
    {
      if (this.NoteService.GetNote(id) == null)
      {
        return RedirectToAction("Index");
      }

      var confirmDeleteAction = new ConfirmDeleteViewModel
      {
        Id = id,
        ActionDelete = "Delete",
        ControllerDelete = "Home",
        ActionRejection = "Index",
        ControllerRejection = "Home"
      };

      return View(confirmDeleteAction);
    }

    public ActionResult Delete(int id)
    {
      this.NoteService.Delete(id);

      return RedirectToAction("Index");
    }

    public ActionResult Create()
    {
      if (DataBaseHealthCheck.IsOverflow())
      {
        var errorModel = new ErrorViewModel
        {
          Message = "Лимит на хранение в базе данных превышен.",
          BackAction = "Index",
          Controller = "Home"
        };

        return View("Error", errorModel);
      }

      return View();
    }

    [HttpPost]
    public ActionResult Create(NoteViewModel request)
    {
      return this.ProceedWithNote(this.NoteService.Create, request);
    }

    public ActionResult Edit(int id)
    {
      var editNote = this.NoteService.GetNote(id);

      if ( editNote == null)
      {
        return RedirectToAction("Index");
      }

      return View("Create", editNote);
    }

    [HttpPost]
    public ActionResult Edit(NoteViewModel request)
    {
      return this.ProceedWithNote(this.NoteService.Update, request);
    }

    private ActionResult ProceedWithNote(Action<NoteViewModel> strategy, NoteViewModel model)
    {
      if (ModelState.IsValid)
      {
        strategy(model);
        return RedirectToAction("Index");
      }

      return View("Create", model);
    }
  }
}