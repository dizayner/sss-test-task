﻿namespace Web.ViewModel.Shared
{
  public class ConfirmDeleteViewModel
  {
    public int Id { get; set; }
    public string ActionDelete { get; set; }
    public string ControllerDelete { get; set; }
    public string ActionRejection { get; set; }
    public string ControllerRejection { get; set; }
  }
}