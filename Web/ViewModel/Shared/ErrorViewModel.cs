﻿namespace Web.ViewModel.Shared
{
  public class ErrorViewModel
  {
    public string Message { get; set; }
    public string BackAction { get; set; }
    public string Controller { get; set; }
  }
}