namespace Data.Model
{
  public enum StatusCode
  {
    Na,
    New,
    Updated,
    Deleted,
  }
}