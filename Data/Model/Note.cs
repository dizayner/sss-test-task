namespace Data.Model
{
  public class Note: IEntity
  {
    public int Id { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public string CreatedDate { get; set; }
    public string UpdatedDate { get; set; }
    public bool Synchronized { get; set; }
    public StatusCode StatusCode { get; set; }
    public string Guid { get; set; }
  }
}