﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Data.Model;

namespace Data.Repositrory
{
  public interface IRepository<TEntity> where TEntity : IEntity
  {
    TEntity GetById(int id);
    void Delete(int id);
    void Create(TEntity entity);
    void Update(TEntity entity);
    IQueryable<TEntity> GetAll();
    IQueryable<TEntity> Filter(Expression<Func<TEntity, bool>> condition);
  }
}
