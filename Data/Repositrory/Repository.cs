using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Data.Model;

namespace Data.Repositrory
{
  public class Repository<TEntity> : IRepository<TEntity> where TEntity : class, IEntity
  {
    private DbContext _dbContext;

    public Repository(DbContext dbContext)
    {
      _dbContext = dbContext;
    }

    public TEntity GetById(int id)
    {
      var entity = _dbContext.Set<TEntity>().FirstOrDefault(x => x.Id == id);

      return entity;
    }

    public void Delete(int id)
    {
      var entity = this.GetById(id);

      if (entity != null)
      {
        var entry = _dbContext.Entry<TEntity>(entity);
        entry.State = EntityState.Deleted;
        
        this.SaveChanges();
      }
    }

    public void Create(TEntity entity)
    {
      _dbContext.Set<TEntity>().Add(entity);

      this.SaveChanges();
    }

    public void Update(TEntity entity)
    {
      _dbContext.Entry<TEntity>(entity).State = EntityState.Modified;
      
      this.SaveChanges();
    }

    public IQueryable<TEntity> GetAll()
    {
      var allEntities = _dbContext.Set<TEntity>();

      return allEntities;
    }

    public IQueryable<TEntity> Filter(Expression<Func<TEntity, bool>> condition)
    {
      var allEntities = condition ==null ? this.GetAll() :  _dbContext.Set<TEntity>().Where(condition);

      return allEntities;
    }

    private void SaveChanges()
    {
      _dbContext.SaveChanges();
    }
  }
}