﻿using System.Data.Entity;
using Data.Model;

namespace Data
{
  public class DataLocalContext : DbContext
  {
    public DataLocalContext():base("DataLocalContext")
    {
      
    }
    public IDbSet<Note> Notes { get; set; }
  }
}
